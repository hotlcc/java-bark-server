package com.hotlcc.bark.service;

import com.hotlcc.bark.service.dto.DevicesDTO;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Validated
public interface DevicesService {
    DevicesDTO save(@NotBlank(message = "device key is empty") String deviceKey, @NotBlank(message = "device token is empty") String deviceToken);

    DevicesDTO queryOne(@NotBlank(message = "device key is empty") String deviceKey);
}
