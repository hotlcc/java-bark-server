package com.hotlcc.bark.service;

import com.hotlcc.bark.service.dto.PushParamsDTO;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Validated
public interface ApplePushService {
    void push(@NotBlank(message = "device key is empty") String deviceKey,
              String category, String title, String body,
              PushParamsDTO pushParams);
}
