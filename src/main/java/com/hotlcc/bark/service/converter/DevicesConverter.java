package com.hotlcc.bark.service.converter;

import com.hotlcc.bark.controller.vo.DevicesVO;
import com.hotlcc.bark.dao.entity.Devices;
import com.hotlcc.bark.service.dto.DevicesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DevicesConverter {
    DevicesConverter INSTANCE = Mappers.getMapper(DevicesConverter.class);

    DevicesDTO convertToDevicesDTO(Devices entity);

    List<DevicesDTO> convertToDevicesDTOList(List<Devices> list);

    @Mapping(target = "key", source = "key")
    @Mapping(target = "deviceKey", source = "key")
    @Mapping(target = "deviceToken", source = "token")
    DevicesVO convertToDevicesVO(DevicesDTO dto);

    List<DevicesVO> convertToDevicesVOList(List<DevicesDTO> list);
}
