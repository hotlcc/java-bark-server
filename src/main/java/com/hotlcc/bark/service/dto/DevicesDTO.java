package com.hotlcc.bark.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DevicesDTO implements Serializable {
    private static final long serialVersionUID = -8986227192031442171L;

    private String key;
    private String token;
}
