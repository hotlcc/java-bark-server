package com.hotlcc.bark.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 推送参数
 */
@Data
public class PushParamsDTO implements Serializable {
    private static final long serialVersionUID = -5565100941303420939L;

    private String sound;
    private Integer isArchive;
    private String icon;
    private String group;
    private String level;
    private String url;
    private String copy;
    private Integer badge;
    private Integer autoCopy;
}
