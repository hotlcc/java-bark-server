package com.hotlcc.bark.controller;

import cn.hutool.core.map.MapUtil;
import com.hotlcc.bark.common.aop.ResponseBodyAdviceImpl;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Misc Controller
 */
@RestController
@Validated
public class MiscController {
    @GetMapping("/ping")
    public void ping() {
    }

    @GetMapping("/healthz")
    @ResponseBodyAdviceImpl.Unsupported
    public String healthZ() {
        return "ok";
    }

    @GetMapping("/info")
    @ResponseBodyAdviceImpl.Unsupported
    public Map<Object, Object> info() {
        return MapUtil.builder()
            .put("version", "v2.0.0")
            .build();
    }
}
