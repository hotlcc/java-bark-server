package com.hotlcc.bark.controller;

import com.hotlcc.bark.service.ApplePushService;
import com.hotlcc.bark.service.dto.PushParamsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 推送 Controller
 */
@RestController
@Validated
public class PushController {
    @Autowired
    private ApplePushService applePushService;

    @RequestMapping(value = "/{deviceKey}", method = {RequestMethod.GET, RequestMethod.POST})
    public void push(@PathVariable("deviceKey") String deviceKey,
                     PushParamsDTO pushParams) {
        applePushService.push(deviceKey, null, null, null, pushParams);
    }

    @RequestMapping(value = "/{deviceKey}/{body}", method = {RequestMethod.GET, RequestMethod.POST})
    public void push(@PathVariable("deviceKey") String deviceKey,
                     @PathVariable("body") String body,
                     PushParamsDTO pushParams) {
        applePushService.push(deviceKey, null, null, body, pushParams);
    }

    @RequestMapping(value = "/{deviceKey}/{title}/{body}", method = {RequestMethod.GET, RequestMethod.POST})
    public void push(@PathVariable("deviceKey") String deviceKey,
                     @PathVariable("title") String title, @PathVariable("body") String body,
                     PushParamsDTO pushParams) {
        applePushService.push(deviceKey, null, title, body, pushParams);
    }

    @RequestMapping(value = "/{deviceKey}/{category}/{title}/{body}", method = {RequestMethod.GET, RequestMethod.POST})
    public void push(@PathVariable("deviceKey") String deviceKey,
                     @PathVariable("category") String category, @PathVariable("title") String title, @PathVariable("body") String body,
                     PushParamsDTO pushParams) {
        applePushService.push(deviceKey, category, title, body, pushParams);
    }
}
