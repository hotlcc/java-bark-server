package com.hotlcc.bark.common.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order
@Slf4j
public class StartupSuccessRunner implements ApplicationRunner {
    @Autowired
    private ServerProperties serverProperties;

    @Override
    public void run(ApplicationArguments args) {
        log.info("应用启动成功，点击访问：http://localhost:{}", serverProperties.getPort());
    }
}
