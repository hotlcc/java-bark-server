package com.hotlcc.bark.common.constant;

import java.util.Date;

public interface CommonConstant {
    interface REQUEST_HEADER {
        String TRACE_ID = "X-Trace-Id";
    }

    interface RESPONSE_HEADER {
        String TRACE_ID = "X-Trace-Id";
    }

    interface LOG_MDC {
        String TRACE_ID = "X_TRACE_ID";
    }

    interface DATE_TIME {
        Date MIN = new Date(0);
    }
}
