package com.hotlcc.bark.common.aop;

import cn.hutool.core.collection.CollUtil;
import com.hotlcc.bark.common.bean.RestResult;
import com.hotlcc.bark.common.exception.ApplicationException;
import com.hotlcc.bark.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult handleException(BusinessException e) {
        log.error("业务异常", e);
        return RestResult.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(ApplicationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult handleException(ApplicationException e) {
        log.error("应用异常", e);
        return RestResult.error(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult handleException(IllegalArgumentException e) {
        log.warn("参数校验异常", e);
        return RestResult.error(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult handleException(MethodArgumentNotValidException e) {
        log.warn("参数校验异常", e);
        StringBuffer sb = new StringBuffer();
        sb.append("参数校验异常：");
        List<ObjectError> errorList = e.getBindingResult().getAllErrors();
        if (CollUtil.isNotEmpty(errorList)) {
            sb.append(
                CollUtil.join(
                    errorList.stream()
                        .filter(Objects::nonNull)
                        .map(ObjectError::getDefaultMessage)
                        .collect(Collectors.toList()),
                    ";"
                )
            );
        }
        return RestResult.error(sb.toString());
    }

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult handleException(RuntimeException e) {
        log.error("运行时异常", e);
        return RestResult.error(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public RestResult handleException(HttpRequestMethodNotSupportedException e) {
        log.error("不支持的方法", e);
        return RestResult.error(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public RestResult handleException(Exception e) {
        log.error("系统异常", e);
        return RestResult.error(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public RestResult handleException(Throwable e) {
        log.error("系统异常", e);
        return RestResult.error(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
