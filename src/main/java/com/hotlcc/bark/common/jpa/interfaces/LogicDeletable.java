package com.hotlcc.bark.common.jpa.interfaces;

import java.io.Serializable;

/**
 * 可逻辑删除的
 */
public interface LogicDeletable<DEL extends Serializable> {
    DEL getDeleted();

    void setDeleted(DEL deleted);
}
