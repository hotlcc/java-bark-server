package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractPersistable;
import lombok.Data;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class PersistableBaseEntity extends AbstractPersistable<Long> {
    private static final long serialVersionUID = -2969323118696529824L;
}
