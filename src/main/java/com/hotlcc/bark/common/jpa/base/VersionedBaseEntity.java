package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractVersioned;
import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class VersionedBaseEntity extends AbstractVersioned<Long, Date, Long, Integer> {
    private static final long serialVersionUID = -2969323118696529824L;
}
