package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractLogicDeletable;
import lombok.Data;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Date;

@SuppressWarnings("Lombok")
@Data
@MappedSuperclass
@EntityListeners(LogicDeletableBaseEntity.LogicDeletableBaseEntityListener.class)
public abstract class LogicDeletableBaseEntity extends AbstractLogicDeletable<Long, Date, Long, Integer> {
    private static final long serialVersionUID = -7007261628850536107L;

    @Configurable
    public static class LogicDeletableBaseEntityListener {
        @PrePersist
        private void prePersist(Object obj) {
            if (obj == null) {
                return;
            }

            if (obj instanceof LogicDeletableBaseEntity) {
                LogicDeletableBaseEntity entity = (LogicDeletableBaseEntity) obj;
                if (entity.getDeleted() == null) {
                    entity.setDeleted(0L);
                }
            }
        }
    }
}