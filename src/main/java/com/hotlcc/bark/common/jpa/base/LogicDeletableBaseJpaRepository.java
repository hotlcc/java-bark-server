package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractLogicDeletable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;

@NoRepositoryBean
public interface LogicDeletableBaseJpaRepository<T extends AbstractLogicDeletable<Long, Date, Long, Integer>> extends VersionedBaseJpaRepository<T> {
}
