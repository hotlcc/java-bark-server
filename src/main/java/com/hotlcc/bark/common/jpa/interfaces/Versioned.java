package com.hotlcc.bark.common.jpa.interfaces;

import java.io.Serializable;

/**
 * 版本化的
 *
 * @param <VER> 版本
 */
public interface Versioned<VER extends Serializable> {
    VER getVersion();

    void setVersion(VER version);
}
