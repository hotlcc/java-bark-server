package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractVersioned;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;

@NoRepositoryBean
public interface VersionedBaseJpaRepository<T extends AbstractVersioned<Long, Date, Long, Integer>> extends AuditableBaseJpaRepository<T> {
}
