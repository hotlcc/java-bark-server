package com.hotlcc.bark.common.jpa.interfaces;

import java.io.Serializable;

/**
 * 可审计的
 *
 * @param <DT>  日期
 * @param <UID> 用户ID
 */
public interface Auditable<DT extends Serializable, UID extends Serializable> {
    DT getCreatedTime();

    void setCreatedTime(DT createdTime);

    UID getCreatedBy();

    void setCreatedBy(UID createdBy);

    DT getUpdatedTime();

    void setUpdatedTime(DT updatedTime);

    UID getUpdatedBy();

    void setUpdatedBy(UID updatedBy);
}
