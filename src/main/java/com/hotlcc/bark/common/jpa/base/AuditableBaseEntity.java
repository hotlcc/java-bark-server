package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractAuditable;
import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class AuditableBaseEntity extends AbstractAuditable<Long, Date, Long> {
    private static final long serialVersionUID = -2969323118696529824L;
}
