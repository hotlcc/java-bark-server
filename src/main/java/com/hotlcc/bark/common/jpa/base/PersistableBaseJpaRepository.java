package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PersistableBaseJpaRepository<T extends AbstractPersistable<Long>> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
}
