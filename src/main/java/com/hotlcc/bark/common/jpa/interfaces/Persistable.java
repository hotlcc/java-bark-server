package com.hotlcc.bark.common.jpa.interfaces;

import java.io.Serializable;

/**
 * 可持久的
 *
 * @param <ID> id
 */
public interface Persistable<ID extends Serializable> extends Serializable {
    ID getId();

    void setId(ID id);
}