package com.hotlcc.bark.common.jpa.abstracts;

import com.hotlcc.bark.common.jpa.interfaces.LogicDeletable;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class AbstractLogicDeletable<ID extends Serializable, DT extends Serializable, UID extends Serializable, VER extends Serializable> extends AbstractVersioned<ID, DT, UID, VER> implements LogicDeletable<ID> {
    private static final long serialVersionUID = -5346470574865899828L;

    @Column(name = "_deleted", nullable = false)
    private ID deleted;
}
