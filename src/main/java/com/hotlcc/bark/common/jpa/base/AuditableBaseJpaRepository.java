package com.hotlcc.bark.common.jpa.base;

import com.hotlcc.bark.common.jpa.abstracts.AbstractAuditable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;

@NoRepositoryBean
public interface AuditableBaseJpaRepository<T extends AbstractAuditable<Long, Date, Long>> extends PersistableBaseJpaRepository<T> {
}
