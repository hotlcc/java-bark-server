package com.hotlcc.bark.common.jpa.abstracts;

import com.hotlcc.bark.common.jpa.interfaces.Auditable;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class AbstractAuditable<ID extends Serializable, DT extends Serializable, UID extends Serializable> extends AbstractPersistable<ID> implements Auditable<DT, UID> {
    private static final long serialVersionUID = 5946449147895982129L;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "_created_time", nullable = false, updatable = false)
    private DT createdTime;

    @Column(name = "_created_by", updatable = false)
    private UID createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "_updated_time", nullable = false)
    private DT updatedTime;

    @Column(name = "_updated_by")
    private UID updatedBy;
}