package com.hotlcc.bark.common.jpa.abstracts;

import com.hotlcc.bark.common.jpa.interfaces.Persistable;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class AbstractPersistable<ID extends Serializable> implements Persistable<ID>, Serializable {
    private static final long serialVersionUID = -2969323118696529824L;

    /**
     * 自增主键
     * <p>
     * 使用数据库本地策略生成主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "_id")
    private ID id;
}
