package com.hotlcc.bark.common.jpa.abstracts;

import com.hotlcc.bark.common.jpa.interfaces.Versioned;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class AbstractVersioned<ID extends Serializable, DT extends Serializable, UID extends Serializable, VER extends Serializable> extends AbstractAuditable<ID, DT, UID> implements Versioned<VER> {
    private static final long serialVersionUID = 5773178199202253121L;

    @Version
    @Column(name = "_version", nullable = false)
    private VER version;
}