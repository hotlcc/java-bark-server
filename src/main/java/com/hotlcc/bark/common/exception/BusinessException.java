package com.hotlcc.bark.common.exception;

import com.hotlcc.bark.common.bean.RestResult;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BusinessException extends ApplicationException {
    private static final long serialVersionUID = 6267883566475036495L;

    @Getter
    private int code;

    private void setCode(int code) {
        if (code == RestResult.SUCCESS_CODE) {
            code = RestResult.DEFAULT_ERROR_CODE;
            log.warn("The value of parameter 'code' be replaced to DEFAULT_ERROR_CODE.");
        }
        this.code = code;
    }

    public BusinessException(int code, String message) {
        super(message);
        setCode(code);
    }

    public BusinessException(String message) {
        this(RestResult.DEFAULT_ERROR_CODE, message);
    }
}
