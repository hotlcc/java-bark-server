package com.hotlcc.bark.common.exception;

public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 4344000949498383631L;

    public ApplicationException() {
        super();
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }
}
