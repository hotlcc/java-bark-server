package com.hotlcc.bark.common.config.properties;

import com.eatthepath.pushy.apns.ApnsClientBuilder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.unit.DataSize;

import java.io.Serializable;

/**
 * 苹果推送服务客户端 Properties
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "apns-client")
public class ApnsClientProperties implements Serializable {
    private static final long serialVersionUID = 2170173181108196393L;

    /**
     * 平台推送服务器
     */
    private String apnsServer = ApnsClientBuilder.PRODUCTION_APNS_HOST;
    /**
     * pkcs8文件路径
     */
    private final SigningKey signingKey = new SigningKey();
    /**
     * 并行连接数
     */
    private int concurrentConnections = 4;
    /**
     * 线程数
     */
    private int threadCount = 4;
    /**
     * 消息主题
     */
    private String topic = "me.fin.bark";
    /**
     * 消息大小限制
     */
    private DataSize payloadMaximum = DataSize.ofBytes(4096L);

    @Setter
    @Getter
    public static class SigningKey implements Serializable {
        private static final long serialVersionUID = 5377228522929562100L;

        /**
         * pkcs8文件路径
         */
        private String pkcs8File = "classpath:META-INF/apns/auth-key.p8";
        private String teamId = "5U8LBRXG3A";
        private String keyId = "LH4T9V5U4R";
    }
}
