package com.hotlcc.bark.dao;

import com.hotlcc.bark.common.jpa.base.LogicDeletableBaseJpaRepository;
import com.hotlcc.bark.dao.entity.Devices;
import org.springframework.stereotype.Repository;

@Repository
public interface DevicesJpaRepository extends LogicDeletableBaseJpaRepository<Devices> {
    Devices getOneByKey(String key);
}
