package com.hotlcc.bark.dao.entity;

import com.hotlcc.bark.common.jpa.base.LogicDeletableBaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@SuppressWarnings("AlibabaClassMustHaveAuthor")
@Data
@Entity(name = "t_devices")
@Table(
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"key", "_deleted"})
    }
)
@SQLDelete(sql = "UPDATE t_devices SET _deleted = _id WHERE _id = ? AND _version = ?")
@Where(clause = "_deleted = 0")
public class Devices extends LogicDeletableBaseEntity {
    private static final long serialVersionUID = -8986227192031442171L;

    /**
     * 推送Key
     */
    @Column(name = "key", nullable = false, updatable = false)
    private String key;
    /**
     * 设备Token
     */
    @Column(name = "token", nullable = false)
    private String token;
}
