package com.hotlcc.bark;

import cn.hutool.extra.spring.EnableSpringUtil;
import com.hotlcc.bark.common.util.TraceUtil;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableSpringUtil
@EnableAsync
@EnableCaching
public class Application {
    public static void main(String[] args) {
        TraceUtil.startTrace();
        SpringApplication application = new SpringApplication(Application.class);
        application.setBannerMode(Mode.OFF);
        application.run(args);
    }
}
