# Java Bark Server

[![Bark Server](https://img.shields.io/badge/Github-Bark%20Server-blank.svg)](https://github.com/Finb/bark-server)
[![License](https://img.shields.io/badge/License-Apache%20License%202.0-blue.svg)](LICENSE)
[![Version](https://img.shields.io/badge/Version-v1.0.0-blank.svg)](https://gitee.com/hotlcc/java-bark-server)

#### 1、介绍

`Bark Server` 是一个基于 `APNs` 的第三方 iOS 推送服务端。原版基于 Go 开发，仓库地址：https://github.com/Finb/bark-server

Java Bark Server 是基于原版基础的 Java 版实现。

#### 2、重要配置

##### 2.1、数据库配置

> 目前支持 SQLite、MySQL。

默认为SQLite：

```properties
spring.datasource.driver-class-name=org.sqlite.JDBC
spring.datasource.url=jdbc:sqlite:bark-server.db
spring.jpa.database-platform=org.sqlite.hibernate.dialect.SQLiteDialect
```

MySQL配置示例：

```properties
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/app_bark?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8
spring.datasource.username=root
spring.datasource.password=123456
```

##### 2.2、APNs 客户端配置

```properties
# Apple 推送服务器
apns-client.apns-server=api.push.apple.com
# p8 文件路径，支持系统文件路径和classpath路径
apns-client.signing-key.pkcs8-file=classpath:META-INF/apns/auth-key.p8
apns-client.signing-key.team-id=5U8LBRXG3A
apns-client.signing-key.key-id=LH4T9V5U4R
# 并行连接数
apns-client.concurrent-connections=4
# 线程数
apns-client.thread-count=4
# 通知主题
apns-client.topic=me.fin.bark
# 消息大小限制
apns-client.payload-maximum=4096B
```

- 以上配置均源自原版 Bark Server 的默认配置，一般无需改动即可直接使用。

#### 3、如何启动

开发环境执行 `com.hotlcc.bark.Application#main` 方法启动。

部署后通过 java 命令启动：

```shell
java -jar java-bark-sever
```
